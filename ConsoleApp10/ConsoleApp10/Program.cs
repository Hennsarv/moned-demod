﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;
using System.Xml.Serialization;
using System.IO;
using System.Xml.Linq;
using System.Xml;


namespace ConsoleApp10
{
    class Program
    {
        static void Main(string[] args)
        {
            Inimene henn = new Inimene { EesNimi = "Henn", PereNimi = "Sarv", Vanus = 62 };
            Inimene ants = new Inimene { EesNimi = "Ants", PereNimi = "Saunamees", Vanus = 40 };
            XmlSerializer xs = new XmlSerializer(typeof(Inimene));
            StringWriter sw = new StringWriter();
            xs.Serialize(sw, henn);
            string hennString = sw.ToString();
            Console.WriteLine(hennString);

            List<Inimene> kaks = new List<Inimene>();

            kaks.Add(henn);
            kaks.Add(ants);

            XmlSerializer xs2 = new XmlSerializer(typeof(List<Inimene>));
            StringWriter sw2 = new StringWriter();
            xs2.Serialize(sw2, kaks);
            string kaksString = sw2.ToString();
            Console.WriteLine(kaksString);

            StringReader sr = new StringReader(hennString);
            Inimene i = (Inimene)xs.Deserialize(sr);
            Console.WriteLine($"Nimi:{i.EesNimi} {i.PereNimi} Vanus: {i.Vanus} ");

            StringReader sr2 = new StringReader(kaksString);
            ((IEnumerable<Inimene>)xs2.Deserialize(sr2))
                .ToList()
                .ForEach(
                x => Console.WriteLine($"Nimi:{x.EesNimi} {x.PereNimi} Vanus: {x.Vanus} ")
                );

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(hennString);

            XmlNodeList xnl = doc.SelectNodes("//*");
            foreach(XmlNode x in xnl)
                Console.WriteLine("{0} := {1} - {2}", x.Name, x.InnerText, x.InnerXml);
            
        }
    }

    [XmlRoot("Person")]
    public class Inimene
    {
        [XmlElement("FirstName")]
        public string EesNimi;
        [XmlElement("LastName")]
        public string PereNimi;
        [XmlAttribute("Age")]
        public int Vanus;
    }
}
