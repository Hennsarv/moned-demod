﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp12
{

    // http://currencyconverter.kowabunga.net/converter.asmx
    class Program
    {
        static void Main(string[] args)
        {
            Converter.ConverterSoapClient c = new Converter.ConverterSoapClient("ConverterSoap");
            
            c.GetCurrencies().ToList()
                .ForEach(
                x => Console.WriteLine(x)
                );
            Console.WriteLine(
            c.GetConversionAmount("USD", "EUR", DateTime.Parse("1.1.2017"), 1)
            ); 
        }
    }
}
