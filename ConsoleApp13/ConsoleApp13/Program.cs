﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;
using System.IO;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace ConsoleApp13
{
    class Program
    {
        static void Main(string[] args)
        {
            string url = @"http://services.groupkt.com/country/get/all";
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
            req.Method = "GET";

            WebResponse resp = req.GetResponse();
            Stream webStream = resp.GetResponseStream();
            StreamReader r = new StreamReader(webStream);
            string response = r.ReadToEnd();
            //Console.WriteLine(response);

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url);

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            var responsex = client.GetStringAsync(url).Result;
            //Console.WriteLine(responsex);

            WebClient c = new WebClient();
            string rc = c.DownloadString(url);
            var re = JsonConvert.DeserializeObject<RestResponse>(rc);

            Console.WriteLine(rc);

        }
    }

    class Country
    {
        public string name;
        public string alpha2_code;
        public string alpha3_code;
    }

    class RestResponse
    {
        public string[] messages;
        public List<Country> result;
    }
}
