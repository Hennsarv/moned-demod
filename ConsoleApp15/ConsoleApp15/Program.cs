﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ConsoleApp15
{
    class Program
    {
        static void Main(string[] args)
        {
            string url = @"http://services.groupkt.com/country/get/all";
            dynamic d = CallRestService(url, "GET", null);

            Console.WriteLine(d);

            Console.WriteLine(d.RestResponse.messages.Count);

            foreach(string x in d.RestResponse.messages)
                Console.WriteLine(x);

            foreach(dynamic v in d.RestResponse.result)
                Console.WriteLine(v.name);

            ((JArray)d.RestResponse.result)
                //.Select( v => new Country
                //{
                //    alpha2_code = v["alpha2_code"].ToString(),
                //    alpha3_code = v["alpha3_code"].ToString(),
                //    name = v["name"].ToString(),
                //}
                //)
                .Select( v => v.ToObject<Country>())
                .ToList()
                .ForEach(v => Console.WriteLine($"{v.alpha2_code} {v.alpha3_code} {v.name}"));


            //foreach( var v in countries)
            //    Console.WriteLine($"{v.alpha2_code} {v.alpha3_code} {v.name}");

        }


        class Country
        {
            public string name;
            public string alpha2_code;
            public string alpha3_code;

        }

        private static dynamic CallRestService(string uri, string method, dynamic parms)
        {
            dynamic result;

            var req = HttpWebRequest.Create(uri);
            req.Method = method;
            req.ContentType = "application/json";
            //byte[] bytes = UTF8Encoding.UTF8.GetBytes(parms.ToString());
            //req.ContentLength = bytes.Length;

            //using (var stream = req.GetRequestStream())
            //{
            //    stream.Write(bytes, 0, bytes.Length);
            //}

            using (var resp = req.GetResponse())
            {
                var results = new StreamReader(resp.GetResponseStream()).ReadToEnd();
                result = JObject.Parse(results);
            }

            return result;
        }

    }
}
