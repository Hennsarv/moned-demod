﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ConsoleApp11
{
    class Program
    {
        static void Main(string[] args)
        {
            Inimene i = new Inimene { Nimi = "Henn", Vanus = 62 };
            string iJson = JsonConvert.SerializeObject(i);
            Console.WriteLine(iJson);

            List<Inimene> inimesed = new List<Inimene>
            {
                new Inimene {Nimi = "Henn", Vanus = 62},
                new Inimene {Nimi = "Ants", Vanus = 40}
            };

            string inimesedJason = JsonConvert.SerializeObject(inimesed);
            Console.WriteLine(inimesedJason);

            Inimene ix = JsonConvert.DeserializeObject<Inimene>(iJson);
            Console.WriteLine($"{ix.Nimi} - {ix.Vanus}");

            var ixs = JsonConvert.DeserializeObject<List<Inimene>>(inimesedJason);
            foreach(var x in ixs)
                Console.WriteLine($"{x.Nimi}:{x.Vanus}");


        }
    }

    class Inimene
    {
        public string Nimi;
        public int Vanus;
    }
}
